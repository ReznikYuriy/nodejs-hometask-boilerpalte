const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getAll() {
        const items = FighterRepository.getAll();
        if (!items) {
            throw new Error('Fighter not found');
        }
        return items;
    }

    getFighter(id) {
        const user = this.search({ id });
        if (!user) {
            throw new Error("Fighter not found");
        }
        return user;
    }

    createFighter(data) {
        const { name } = data;
        const _name = this.search({ name });
        if (_name) {
            throw new Error('Fighter with such name already added');
        }
        const item = FighterRepository.create(data);
        if (!item) {
            throw new Error('Error');
        }
        return item;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    updateFighter(id, dataToUpdate) {
        const _id = this.search({ id });
        if (!_id) throw new Error("Fighter not found");
        return FighterRepository.update(id, dataToUpdate);
    }

    deleteFighter(id) {
        const _id = this.search({ id });
        if (!_id) throw new Error("Fighter not found");
        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();