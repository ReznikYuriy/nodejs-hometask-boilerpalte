const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getAll() {
        const items = UserRepository.getAll();
        if (!items) {
            return null;
        }
        return items;
    }

    createUser(data) {
        const { phoneNumber, email } = data;
        const _phoneNumber = this.search({ phoneNumber });
        if (_phoneNumber) {
            throw Error('User with such phone number already added');
        }
        //email=email.toLowerCase();
        const _email = this.search({ email });
        if (_email) {
            throw Error('User with such email already added');
        }
        const item = UserRepository.create(data);
        if (!item) {
            return null;
        }
        return item;
    }
    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
    updateUser(id, dataToUpdate) {
        const _id = this.search({ id })
        if (!_id) throw new Error("UserNotFound")
        return UserRepository.update(id, dataToUpdate);
    }
    deleteUser(id) {
        const _id = this.search({id})
        if(!_id) throw new Error("UserNotFound")
        return UserRepository.delete(id);
    }
}

module.exports = new UserService();