const checkToExtraField = (req, model) => {
    for (const field in req.body) {
        if (!model.hasOwnProperty(field) || field === 'id') throw new Error(`The field '${field}' is excess!`);
    }
};
const checkToMissedField = (req, model) => {
    for (const field in model) {
        if (!req.body.hasOwnProperty(field) && field !== 'id') throw new Error(`The field '${field}' is missed!`);
    }
};

exports.checkToExtraField = checkToExtraField;
exports.checkToMissedField = checkToMissedField;